import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'goalsfilter'
})
export class GoalsfilterPipe implements PipeTransform {

  transform(items: Array<any>, position1: any, position2: any, position3: any, position4: any, active:any) {
    //debugger
    if (items && items.length){
        return items.filter(item =>{
            if (position1 && item.position_name.toLowerCase().indexOf('حارس مرمى') === -1){
                return false;
            }
            if (position2 && item.position_name.toLowerCase().indexOf('مدافع') === -1){
                return false;
            }
            if (position3 && item.position_name.toLowerCase().indexOf('خط وسط') === -1){
                return false;
            }
            if (position4 && item.position_name.toLowerCase().indexOf('مهاجم') === -1){
                return false;
            }
            if (active && item.P_Active.toLowerCase().indexOf('1') === -1){
                return false;
            }
            return true;
       })
    }
    else{
        return items;
    }
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module'
import { GoalsComponent } from './goals.component';
import { GoalsfilterPipe } from './goalsfilter.pipe';
import { GoalsModalComponent } from './goalsmodal.component';


@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
  ],
  declarations: [
  	GoalsComponent,
  	GoalsModalComponent,
  	GoalsfilterPipe,
  ],
  entryComponents: [
    GoalsModalComponent,
  ],
})
export class GoalsModule { }

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GoalsModalComponent } from './goalsmodal.component';

@Component({
  selector: 'goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {
  data: Array<any>;
  team_type = 'Team';
  team_types = ['Team', '1', '2', '3' ];

  goals: object[];
  
  constructor(private http: HttpClient, private modalService: NgbModal) {
        this.getJSON().subscribe(data => {
            this.goals=data;
            console.log(data)
        });
  }

  showLargeModal(url:string) {
    const activeModal = this.modalService.open(GoalsModalComponent, { size: 'lg', container: 'nb-layout' });

    activeModal.componentInstance.modalHeader = 'Large Modal';
    activeModal.componentInstance.modalContent = url;
  }

   public getJSON(): Observable<any> {
      return this.http.get("./assets/JSONData/Goals.json")
   }

  ngOnInit() {
  }

}

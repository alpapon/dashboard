import { Component, NgModule } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'ngx-modal',
  template: `
    <div class="modal-header">
      <button class="close" aria-label="Close" (click)="closeModal()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" [src]="url" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>
  `,
 // styles: ['iframe { width: 100%; }']
})
export class GoalsModalComponent {

  modalHeader: string;
  modalContent: string;
  url;

  constructor(private activeModal: NgbActiveModal, public sanitizer: DomSanitizer) { }

  closeModal() {
    this.activeModal.close();
  }

 ngOnInit() {
      this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.modalContent.replace("watch?v=", "embed/"));
  }
}


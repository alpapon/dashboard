import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss']
})
export class MatchesComponent implements OnInit {
  data: Array<any>;
  team_head = 'الفريق';
  season_head = 'الموسم';
  champtype_head = 'البطولة';
  seasons: string[];
  champtypes: string[];
  teams: string[];
  matches: object[];
  
  dateFrom : Date = new Date("1975-12-15");
  //dateFrom : Date = new Date("2018-03-01");
  dateTo : Date = new Date();
  
  constructor(private http: HttpClient) {
        this.getJSON().subscribe(data => {
            this.matches=data;
			
			/*Teams*/
			const team_temp1 = data.map(data => data.Team_Home_Name);
			const team_temp2 = data.map(data => data.Team_Away_Name);
			
			this.teams = [ ...team_temp1, ...team_temp2];
			
			// Unique teams
			this.teams = this.teams.filter((x, i, a) => x && a.indexOf(x) === i);
			this.teams = this.teams.sort();
	
			/*Seasons*/
			const season = data.map(data => data.C_Season);
			
			// Unique Seasons
			this.seasons = season.filter((x, i, a) => x && a.indexOf(x) === i);
			this.seasons = this.seasons.sort();

			/*Champ Types */
			const champtype = data.map(data => data.CT_Name);
			
			// Unique champs
			this.champtypes = champtype.filter((x, i, a) => x && a.indexOf(x) === i);
			this.champtypes = this.champtypes.sort();
			
            console.log(data);
        });
   }
 
   public getJSON(): Observable<any> {
      return this.http.get("./assets/JSONData/MatchesList.json")
   }

  ngOnInit() {
  }

}

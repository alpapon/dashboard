import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'all-matches',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AllMatchesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

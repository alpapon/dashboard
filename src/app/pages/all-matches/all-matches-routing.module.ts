import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AllMatchesComponent } from './all-matches.component';
import { MatchesComponent } from './matches/matches.component';
import { MatchDetailsComponent } from './match-details/match-details.component';

const routes: Routes = [{
  path: '',
  component: AllMatchesComponent,
  children: [
  {
    path: 'matchdetails/:id',
    component: MatchDetailsComponent,
  },{
    path: 'matches',
    component: MatchesComponent,
  },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllMatchesRoutingModule { }

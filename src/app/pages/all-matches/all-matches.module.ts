import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AllMatchesRoutingModule } from './all-matches-routing.module';
import { AllMatchesComponent } from './all-matches.component';

import { MatchesComponent } from './matches/matches.component';
import { MatchesFilterPipe } from './matches/matches.pipe';

import { MatchDetailsComponent } from './match-details/match-details.component';
import { MatchDetailsFilterPipe } from './match-details/match-details.pipe';

const components = [
  AllMatchesComponent,
  MatchesComponent,
  MatchDetailsComponent,
  MatchesFilterPipe,
  MatchDetailsFilterPipe
];

@NgModule({
  imports: [
    CommonModule,
    AllMatchesRoutingModule,
    ThemeModule,
    FormsModule
  ],
  declarations: [
    ...components,
  ],

})
export class AllMatchesModule { }

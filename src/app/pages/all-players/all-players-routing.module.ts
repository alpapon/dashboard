import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AllPlayersComponent } from './all-players.component';
import { PlayersComponent } from './players/players.component';
import { PlayerdetailsComponent } from './playerdetails/playerdetails.component';

const routes: Routes = [{
  path: '',
  component: AllPlayersComponent,
  children: [
  {
    path: 'playerdetails/:id',
    component: PlayerdetailsComponent,
  },{
    path: 'players',
    component: PlayersComponent,
  },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllPlayersRoutingModule {
}


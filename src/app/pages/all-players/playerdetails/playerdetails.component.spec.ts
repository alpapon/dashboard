import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EachplayerComponent } from './eachplayer.component';

describe('EachplayerComponent', () => {
  let component: EachplayerComponent;
  let fixture: ComponentFixture<EachplayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EachplayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EachplayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

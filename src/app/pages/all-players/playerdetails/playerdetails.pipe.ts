import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'playerdetailsfilter'
})
export class PlayerDetailsFilterPipe implements PipeTransform {
  transform(items: Array<any>, team: any, season: string, champtype: string, dateFromSimple: Date, dateToSimple: Date) {
    let dateFrom= new Date(dateFromSimple);
	let dateTo= new Date(dateToSimple);
    if (items && items.length){
        return items.filter(item =>{
			let mDate = new Date(item.M_Date);
            if (
				//teams/
				(
					(item.Team_Home_Name.toLowerCase().indexOf(team) !== -1)
					||
					(item.Team_Away_Name.toLowerCase().indexOf(team) !== -1)
					||
					(team.toLowerCase().indexOf('الفريق') !== -1)
				)
				&&
				//season
				(
					(item.C_Season.toLowerCase().indexOf(season) !== -1)
					||
					(season.toLowerCase().indexOf('الموسم') !== -1)
				)
				&&
				//champtype
				(
					(item.CT_Name.toLowerCase().indexOf(champtype) !== -1)
					||
					(champtype.toLowerCase().indexOf('البطولة') !== -1)
				)
				&&
				//dates
				(
					(mDate >= dateFrom)
					&&
					(mDate <= dateTo)
				)
			){
                return true;
            }
            return false;
       })
    }
    else{
        return items;
    }
  }

}

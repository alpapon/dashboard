import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AllPlayersRoutingModule } from './all-players-routing.module';
import { AllPlayersComponent } from './all-players.component';

import { PlayersComponent } from './players/players.component';
import { PlayerFilterPipe } from './players/players.pipe';

import { PlayerdetailsComponent } from './playerdetails/playerdetails.component';

const components = [
  AllPlayersComponent,
  PlayersComponent,
  PlayerdetailsComponent,
  PlayerFilterPipe
];

@NgModule({
  imports: [
    CommonModule,
    AllPlayersRoutingModule,
    FormsModule,
    ThemeModule
  ],
  declarations: [
    ...components,
  ],

})
export class AllPlayersModule { }



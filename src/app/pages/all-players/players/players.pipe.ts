import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'playerfilter'
})

export class PlayerFilterPipe implements PipeTransform {
	transform(items: Array<any>, positions: Array<any>, active: boolean) {
		//alert (positions);
		/*
		if (!positions || positions.length === 0) return items;
		return items.filter(item => positions.includes(item.position_name));
		*/
	if (items && items.length){
        return items.filter(item =>{
			let activeItem= !!+item.P_Active;
			//console.log('activeItem: ' + activeItem + ' acive: ' + active + '        item.position_name:' + item.position_name);
            if (
				(positions.includes(item.position_name.toLowerCase()))
				&&
				(activeItem == active)
			)
			{
                return true;
            }

            return false;
       })
    }
    else{
        return items;
    }
  }
}
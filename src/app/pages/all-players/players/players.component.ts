import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {

  players: object[];
  positions = [
    {name: 'حارس مرمى', selected:true},
    {name: 'مدافع', selected:true},
    {name: 'خط وسط', selected:true},
	{name: 'مهاجم', selected:true}
  ];
  
  isActive: boolean = true;
  
  constructor(private http: HttpClient) {
      this.getJSON().subscribe(data => {
          this.players=data;
          console.log(data)
      });
  }
 
  public getJSON(): Observable<any> {
      return this.http.get("./assets/JSONData/Players.json")
  }

  get selectedPositions() {
    return this.positions.reduce((types, type) => {
      if (type.selected) {
        types.push(type.name);
      }
      return types;
    }, [])
  }

 
  ngOnInit() {
  }

}

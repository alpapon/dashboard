import { Component } from '@angular/core';

@Component({
  selector: 'all-players',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AllPlayersComponent {
}

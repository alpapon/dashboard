import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'highlighted-player',
  templateUrl: './highlighted-player.component.html',
  styleUrls: ['./highlighted-player.component.scss']
})
export class HighlightedPlayerComponent implements OnInit {
  data: object[];
  constructor(private http: HttpClient) {
        this.getJSON().subscribe(data => {
            this.data=data;
            console.log(data)
        });
   }
 public getJSON(): Observable<any> {
        return this.http.get("./assets/JSONData/HighlightPlayer.json")
    }

  ngOnInit() {
  }

}



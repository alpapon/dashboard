import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighlightedPlayerComponent } from './highlighted-player.component';

describe('HighlightedPlayerComponent', () => {
  let component: HighlightedPlayerComponent;
  let fixture: ComponentFixture<HighlightedPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighlightedPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighlightedPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

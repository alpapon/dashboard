import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NbThemeService } from '@nebular/theme';
import { Observable } from 'rxjs/Observable';

declare const echarts: any;

@Component({
  selector: 'ngx-electricity-chart',
  styleUrls: ['./electricity-chart.component.scss'],
  template: `
    <div echarts [options]="option" class="echart"></div>
  `,
})
export class ElectricityChartComponent implements AfterViewInit, OnDestroy, OnInit {

  option: any;
  tooltip: Array<any>;
  label: Array<any>;
  data: Array<any>;
  themeSubscription: any;

  constructor(private theme: NbThemeService, private http: HttpClient) {
    //debugger


      //echarts.setOption(this.option, true);
  }

  public getJSON(): Observable<any> {//debugger
      return this.http.get("./assets/JSONData/TeamProgress.json")
  }

  ngAfterViewInit(): void {
          this.getJSON().subscribe((data:any)=> {
              this.label = data.result.map(element => {
                 // if you want to return an array of objects
                 //return {"label": element.label};
                 return element.label;
               });

              this.data = data.result.map(element => {
                 // return {"data": element.data};
                 return element.data;
              });

             this.tooltip = data.result.map(element => {
                 //return {"tooltip": element.tooltip};
                 return element.tooltip;
             });
             
      })
          
    this.themeSubscription = this.theme.getJsTheme().delay(1).subscribe(config => {
      const eTheme: any = config.variables.electricity;
      var me = this;
      this.option = {
          grid: {
            left: 20,
            top: 20,
            right: 20,
            bottom: 40,
          },
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'line',
              lineStyle: {
                color: eTheme.tooltipLineColor,
                width: eTheme.tooltipLineWidth,
              },
            },
            textStyle: {
              color: eTheme.tooltipTextColor,
              fontSize: 20,
              fontWeight: eTheme.tooltipFontWeight,
            },
            position: 'top',
            backgroundColor: eTheme.tooltipBg,
            borderColor: eTheme.tooltipBorderColor,
            borderWidth: 3,
            formatter: function (params) {
              // var colorSpan = color => '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + color + '"></span>';
              let rez = '';// '<p>' + params[0].axisValue + '</p>';
              console.log(params); // quite useful for debug
              var i = 0;
              params.forEach(item => {
                  console.log(item); //quite useful for debug
                  const xx = me.tooltip[item.dataIndex];
                  rez += xx;
                  i++;
              });

              return rez;
            } ,
            extraCssText: eTheme.tooltipExtraCss,
          },
          xAxis: {
            data : this.label,
          /*
            type: 'category',
            boundaryGap: false,
            offset: 25,
            data: this.data.map(i => i.label),
            axisTick: {
              show: false,
            },
            axisLabel: {
              textStyle: {
                color: eTheme.xAxisTextColor,
                fontSize: 18,
              },
            },
            axisLine: {
              lineStyle: {
                color: eTheme.axisLineColor,
                width: '2',
              },
            },
          */},
          yAxis: {
            boundaryGap: [0, '5%'],
            axisLine: {
              show: false,
            },
            axisLabel: {
              show: false,
            },
            axisTick: {
              show: false,
            },
            splitLine: {
              show: true,
              lineStyle: {
                color: eTheme.yAxisSplitLine,
                width: '1',
              },
            },
          },
          series: [
            {
              type: 'line',
              smooth: false,
              symbolSize: 20,
              itemStyle: {
                normal: {
                  opacity: 0,
                },
                emphasis: {
                  color: '#ffffff',
                  borderColor: eTheme.itemBorderColor,
                  borderWidth: 2,
                  opacity: 1,
                },
              },
              lineStyle: {
                normal: {
                  width: eTheme.lineWidth,
                  type: eTheme.lineStyle,
                  color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 0,
                    color: eTheme.lineGradFrom,
                  }, {
                    offset: 1,
                    color: eTheme.lineGradTo,
                  }]),
                  shadowColor: eTheme.lineShadow,
                  shadowBlur: 6,
                  shadowOffsetY: 12,
                },
              },
              areaStyle: {
                normal: {
                  color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 0,
                    color: eTheme.areaGradFrom,
                  }, {
                    offset: 1,
                    color: eTheme.areaGradTo,
                  }]),
                },
              },
              data: this.data,
            },
/*
            {
              type: 'line',
              smooth: false,
              symbol: 'none',
              lineStyle: {
                normal: {
                  width: eTheme.lineWidth,
                  type: eTheme.lineStyle,
                  color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 0,
                    color: eTheme.lineGradFrom,
                  }, {
                    offset: 1,
                    color: eTheme.lineGradTo,
                  }]),
                  shadowColor: eTheme.shadowLineDarkBg,
                  shadowBlur: 14,
                  opacity: 1,
                },
              },
              data: this.data.map(i => i.value),
            },*/
          ],
        };
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }
}

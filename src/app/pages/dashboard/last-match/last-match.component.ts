import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'last-match',
  templateUrl: './last-match.component.html',
  styleUrls: ['./last-match.component.scss']
})
export class LastMatchComponent implements OnInit {

  data: object[];
  constructor(private http: HttpClient) {
        this.getJSON().subscribe(data => {
            this.data=data;
            console.log(data)
        });
   }
 public getJSON(): Observable<any> {
        return this.http.get("./assets/JSONData/LastMatch.json")
    }

  public getDayOfWeek(date): string {
    let dayOfWeek = new Date(date).getDay();    
    return isNaN(dayOfWeek) ? null : ['الأحد', 'الأثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة', 'السبت'][dayOfWeek];
  }

  ngOnInit() {
  }

}

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'next-match',
  templateUrl: './next-match.component.html',
  styleUrls: ['./next-match.component.scss']
})
export class NextMatchComponent implements OnInit {

  data: object[];
  constructor(private http: HttpClient) {
        this.getJSON().subscribe(data => {
            this.data=data;
            console.log(data)
        });
   }
 public getJSON(): Observable<any> {
        return this.http.get("./assets/JSONData/NextMatch.json")
    }

  public getDayOfWeek(date): string {
    let dayOfWeek = new Date(date).getDay();    
    return isNaN(dayOfWeek) ? null : ['الأحد', 'الأثنين', 'الثلاثاء', 'الأربعاء', 'الخميس', 'الجمعة', 'السبت'][dayOfWeek];
  }

  ngOnInit() {
  }

}

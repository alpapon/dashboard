import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AllPlayersComponent } from './all-players/all-players.component';
import { AllMatchesComponent } from './all-matches/all-matches.component';
import { GoalsComponent } from './goals/goals.component';




const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
    path: 'all-players',
    loadChildren: './all-players/all-players.module#AllPlayersModule',
  }/*, {
    path: 'matches',
    component: MatchesComponent,
  }*/,{
    path: 'goals',
    component: GoalsComponent,
  },{
    path: 'all-matches',
    loadChildren: './all-matches/all-matches.module#AllMatchesModule',
  },/* {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, {
    path: 'editors',
    loadChildren: './editors/editors.module#EditorsModule',
  }, {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  }, {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule',
  },*/ {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',

  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
